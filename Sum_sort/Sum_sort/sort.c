#include"sort.h"
void Printarray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d", a[i]);
	}
	printf("\n");
}
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		int end = i - 1;
		int tmp = a[i];
		//将tmp插入到【0，end】区间中，保持有序
		//两种情况
		while (end >= 0) 
		{

			if (tmp < a[end])
			{
				a[end + 1] = a[end];
				--end;
			}
			else			
				break;		
		}	
		a[end + 1] = tmp;

	}
}
void ShellSort(int* a, int n)
{
	//int gap = 3; 
	////一组排完再排另一组
	//for (int j = 0; j < gap; j++)
	//{
	//	for (int i = j; i < n-gap; i += gap)
	//	{
	//		int end = i ;
	//		int tmp = a[i+gap];
	//		while (end >= 0)
	//		{
	//			if (tmp < a[end])
	//			{
	//				a[end + gap] = a[end];
	//				end -= gap;
	//			}
	//			else
	//				break;
	//		}

	//		a[end + gap] = tmp;
	//	}
	//}
	//一次排完
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[i + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
					break;
			}

			a[end + gap] = tmp;
		}
	}

}

void Swap(int* p1, int* p2)
{
	int temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}
void SelectSort(int* a, int n)
{
	int left = 0, right = n - 1;
	while (left < right)
	{
		int mini = left, maxi = left;
		for (int i = left + 1; i <= right; i++)
		{
			if (a[i] < a[mini])
			{
				mini = i;
			}
			if (a[i] > a[maxi])
			{
			 	maxi = i;
			}
		}
		Swap(&a[left], &a[mini]);
		//如果left和maxi重叠，交换后需要修整一下
		if (left == maxi)
		{
			maxi = mini;
		}

		Swap(&a[right], &a[maxi]);
		++left;
		--right;
	}
}
//建小堆
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2+1;
	while (child < n)
	{
		//选出左右孩子中较大的那一个
		if(child+1<n&&a[child+1]<a[child])
		{
			child++;
		}
		if (a[child > a[parent]])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;

	}
}


//输出的是降序
void HeapSort(int* a, int n)
{
	//建堆，向下调整建堆--O（N）
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	//排序
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[end], &a[0]);
		AdjustDown(a, n, 0);
		--end;
	}

}
void BubbleSort(int* a, int n)
{
	for (int j = 0; j < n; j++)
	{
		int exchange = 0;
		for (int i = 1; i < n-j; i++)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = 1;
			}
		}
		if (exchange == 0)
			break;
	}
}
int GetMidNum(int* a, int left, int right)
{
	int mid =( left + right) / 2;
	if (a[left] < mid)
	{
		if (a[mid] < right)
			return mid;
		else if ((a[left] > right))
			return left;
		else
			return right;
	}
	else
	{
		if (a[mid] > a[right])
			return mid;
		else if (a[left] < a[right])
			return left;
		else
			return right;
	}


}
//Hoar方法
void QuickSort1(int* a, int left, int right)
{
	if (left >= right)
		return;

	int begin = left;
	int end= right;
	//随机选Key；
	//int randi = left + (rand() % (right - left));
	//Swap(&a[left],&a[randi]);
	//三路取中
	int midi = GetMidNum(a, left, right);
	Swap(&a[left], &a[midi]);
	int keyi= left;
	while (left < right)
	{
		//右边找小
		while (left<right&&a[right] >= a[keyi])
			--right;
		//左边找大
		while (left<right&&a[left] <= a[keyi])
			++left;
		Swap(&a[left], &a[right]);
	}
	Swap(&a[keyi], &a[left]);
	keyi = left;
	QuickSort1(a, begin, keyi-1);
	QuickSort1(a, keyi+1,end);
}
//挖坑法
void QuickSort2(int* a, int left, int right)
{
	if (left >= right)
		return;

	int begin = left;
	int end = right;
	//随机选Key；
	//int randi = left + (rand() % (right - left));
	//Swap(&a[left],&a[randi]);
	//三路取中
	int midi = GetMidNum(a, left, right);
	Swap(&a[left], &a[midi]);
	int key = left;
	int hole = left;
	while (left < right)
	{
		//右边找小
		while (left < right && a[right] >= key)
			--right;
		a[hole] = a[right];
		hole = right;
		//左边找大
		while (left < right && a[left] <= key)
			++left;
		a[hole] = a[left];
		hole = left;
	}
	a[hole] = key;

	QuickSort2(a, begin, hole - 1);
	QuickSort2(a, hole + 1, end);
}

int PartSort3(int* a, int left, int right)
{
	if (left >= right)
		return;


	int midi = GetMidNum(a, left, right);
	Swap(&a[left], &a[midi]);
	int key = left;
	int  prev = left;
	int  cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[key] && ++prev != cur)
		{
			Swap(&a[cur], &a[prev]);
		}
		++cur;

	}

	Swap(&a[prev],&a[key]);
	return key;
}
void QuickSort(int* a, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	//小区间优化--小区间直接使用直接插入
	if ((right - left + 1) > 10)
	{
		int keyi = PartSort3(a, left, right);
		QuickSort(a, left, keyi - 1);
		QuickSort(a, keyi + 1, right);
	}
	else
	{
		InsertSort(a + left, right - left + 1);
	}

}
//void QuickSortNonR(int* a, int left, int right)
//{
//	ST st;
//	StackInit(&st);
//	StackPush(&st, right);
//	StackPush(&st, left);
//	while (!StackEmpty())
//	{
//		int begin = StackTop(&st);
//		StackPop(&st);
//		int end = StackTop(&st);
//		StackPop(&st);
//		int keyi = PartSort3(a, begin, end);
//		if (keyi + 1 < end)
//		{
//			StackPush(&st, right);
//			StackPush(&st, keyi+1);
//		}
//		if (begin < keyi-1)
//		{
//			StackPush(&st, keyi);
//			StackPush(&st,begin);
//		}
//
//	}
//
//}
void _MergeSort(int* a, int begin, int end, int* tmp)
{
	if (begin >= end)
		return;

	int mid = (begin + end) / 2;
	// [begin, mid] [mid+1,end]，子区间递归排序
	_MergeSort(a, begin, mid, tmp);
	_MergeSort(a, mid + 1, end, tmp);

	// [begin, mid] [mid+1,end]归并
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int i = begin;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[i++] = a[begin1++];
		}
		else
		{
			tmp[i++] = a[begin2++];
		}
	}

	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}

	memcpy(a + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}



void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail\n");
		return;
	}

	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
}

void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		printf("malloc fail");
		return;
	}
	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i+=gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;

			//一次一次拷贝：
			if (end1 >= n || begin2 >= n)
			{
				break;

			}
			if(end2>=n) {
				end2 = n - 1;
			}


			//整体拷贝：
			/*if (end1 >= n)
			{
				end1 = n - 1;
				begin2 = n;
				end2 = n - 1;
			}
			else if (begin2 >= n)
			{
				begin2 = n;
				end2 = n - 1;
			}
			else if (end2 >= n)
			{
				end2 = n - 1;
			}*/


			int j = i;


			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[j++] = a[begin1++];
				}
				else {
					tmp[j++] = a[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[j++] = a[begin1++];
			}
			while (begin2 <= end1)
			{
				tmp[j++] = a[begin2++];
			}
			memcpy(a+i, tmp+i, sizeof(int) * (end2-i+1));
		}
		printf("\n");
		//整体拷贝
		//memcpy(a, tmp, sizeof(int) * n);
		gap *= 2;
	}
	free(tmp);
}

// 时间复杂度：O(N+range)
// 空间复杂度：O(range)
//此方法可以处理负数
void CountSort(int* a, int n)
{
	int max = a[0], min = a[0];
	for (int i = 1; i < n; ++i)
	{
		if (a[i] > max)
		{
			max = a[i];
		}

		if (a[i] < min)
		{
			min = a[i];
		}
	}

	int range = max - min + 1;
	int* countA = (int*)malloc(sizeof(int) * range);
	if (countA == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	memset(countA, 0, sizeof(int) * range);

	// 计数
	for (int i = 0; i < n; i++)
	{
		countA[a[i] - min]++;
	}

	// 排序
	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (countA[i]--)
		{
			a[j++] = i + min;
		}
	}

	free(countA);
}

