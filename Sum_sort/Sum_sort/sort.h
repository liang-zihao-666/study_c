#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

void InsertSort(int* a, int n);
void Printarray(int* a, int n);
void ShellSort(int* a, int n);
void SelectSort(int* a, int n);
void Swap(int* p1, int* p2);
void AdjustDown(int* a, int n, int parent);
void HeapSort(int* a, int n);
void BubbleSort(int* a, int n);
void PartSort(int* a, int left, int right);
int GetMidNum(int* a, int left, int right);
void QuickSort(int* a, int left, int right);
void QuickSort1(int* a, int left, int right);
void QuickSort2(int* a, int left, int right);
void QuickSort3(int* a, int left, int right);
void MergeSort(int* a, int n);
void MergeSortNonR(int* a, int n);
void CountSort(int* a, int n);