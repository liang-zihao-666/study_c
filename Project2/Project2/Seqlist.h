#pragma once
#include<stdio.h>;
#include<malloc.h>;
#include<stdlib.h>;
#include<assert.h>;

typedef int SLDataType;		//定义通用元素类型
#define N 10
#define INIT_CAPACITY 4
//静态顺序表
//struct Seqlist
//{
//	int a[N];
//	int size;
//};
//动态顺序表--按需申请	
typedef struct Seqlist
{
	SLDataType* a;
	int size; //有效数据个数
	int capacity; //容量
} SL;
//增删查改
void SLInit(SL *ps);
void SLDestroy(SL* ps);
void TestSeqlist(void);
void SLPrint(SL* ps);
void SLCheakCapacity(SL* ps);
void SLPushBack(SL* ps, SLDataType x);
void SLPopBack(SL* ps);
void SLPushFront(SL* ps, SLDataType x);
void SLPopFront(SL* ps);
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);
int SLFind(SL* ps, SLDataType x);