#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
typedef int SLTDataType;
typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
	/*SListNode* next;这两个写法都不可以,第一个C++可以这么写
	SLTNode *next*/
}SLTNode;

void SLTPrint(SLTNode* phead);
SLTNode* BuySLTNode(SLTDataType x);
void SLTPushBack(SLTNode** pphead, SLTDataType x);
void SLTPushFront(SLTNode** pphead, SLTDataType x);
void SLTPopBack(SLTNode** pphead);
void SLTPopFront(SLTNode** pphead);
SLTNode* SLTFind(SLTNode* phead, SLTDataType x);
//pos位置之前插入
void SLTInsert(SLTNode** pphead, SLTNode* pos,SLTDataType x);
//pos位置删除
void SLTErase(SLTNode** pphead, SLTNode* pos);
//pos位置后面插入
void SLTInsertAfter(SLTNode* pos, SLTDataType x);
//pos位置后面删除
void SLTEraseAfter(SLTNode* pos);
void STLDestroy(SLTNode* phead);