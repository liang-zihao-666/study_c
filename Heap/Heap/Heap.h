#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HeapInit(HP* php);
void HeapDestroy(HP* php);
void AdjustUp(HPDataType* a, int child);
void HeapPush(HP* php, HPDataType x);
void AdjustDown(HPDataType* a, int n, int parent);
void HeapPop(HP* php);
void HeapSort(int* a, int n);