#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
 
//初始化数组
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}
 
//打印棋盘
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	printf("----------------扫雷游戏----------------\n");
	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= 9; i++)
	{
		printf("%d ", i);
		int j = 0;
		for (j = 1; j <= 9; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}
 
//布置雷
void SetMine(char board[ROWS][COLS], int row, int col)
{
	//布置10个雷
	//随机生成十个随机坐标，布置雷
	int count = COUNT;
	while (count)
	{
		int x = rand()%row+1;
		int y = rand()%col+1;
		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}
 
 
//统计周围雷的个数
int  GetMineCount(char mine[ROWS][COLS], int x, int y)
{
	return (mine[x - 1][y] + mine[x - 1][y - 1] + mine[x][y - 1] + mine[x + 1][y - 1] + mine[x + 1][y]
		+ mine[x + 1][y + 1] + mine[x][y + 1] + mine[x - 1][y + 1] - 8 * '0');
}
 
 
//爆炸展开
void ExplodeBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int x, int y,int* p)
{
	int temp = *p;
	temp++;
	//限制条件
	if (x >= 1 && x <= row && y >= 1 && y <= col)
	{
		//计算该位置周围雷的个数
		int count = GetMineCount(mine, x, y);
		if (count == 0)
		{
			//把该位置变成空格
			show[x][y] = ' ';
			int i = 0;
			//向周围进行递归遍历
			for (i = x - 1; i <= x + 1; i++)
			{
				int j = 0;
				for (j = y - 1; j <= y + 1; j++)
				{

					//限制对重复递归调用的条件，避免死递归
					if (show[i][j] == '*')
					{
						temp++;
						ExplodeBoard(mine, show, row, col, i, j,&temp);
						
					}
				}
			}
		}
		else
		{
			show[x][y] = count + '0';
		}
	}
}
 
//标记雷的函数
void Signmine(char board[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	while (1)
	{
		printf("请输入要标记的坐标：");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x][y] == '*')
			{
				board[x][y] = '!';
				break;
			}
			else
			{
				printf("该位置不能被标记，请重新输入：\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入：\n");
		}
	}
}
//排查雷的函数
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;
	char ch = 0;
	while (win < ROW*COL-COUNT)
	{
		printf("输入所要排查的目标：\n");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)//检查坐标是否合法
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾，你被炸死了。\n");
				DisplayBoard(mine,ROW,COL);
				break;
			}
			else
			{
				//爆炸展开
			
				ExplodeBoard(mine, show, row, col,x,y,&win);
				//打印棋盘
				DisplayBoard(show, ROW, COL);
				printf("需要标注地雷输入：Y,不需要则输入：N\n");
				//清空缓冲区
				while ((ch = getchar()) != '\n');
				scanf("%c", &ch);
				if (ch == 'Y')
				{
					//标记雷的位置
					Signmine(show, ROW, COL);
				}
			}
		}
		else
		{
			printf("坐标非法，重新输入：\n");
		}
		//打印棋盘
		DisplayBoard(show, ROW, COL);
	}
	if (win == ROW * COL - COUNT)
	{
		printf("恭喜你，排雷成功\n");
		DisplayBoard(mine, ROW, COL);
	}
}