#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

//雷的个数
#define COUNT 10

//初始化数组函数的声明
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set);

//打印棋盘数组的函数声明
void DisplayBoard(char board[ROWS][COLS], int row, int col);

//布置雷的函数声明
void SetMine(char board[ROWS][COLS], int row, int col);

//排查雷的函数声明
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col);