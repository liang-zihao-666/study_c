#pragma once
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef int BTDataType;
// 二叉链
typedef struct BinaryTreeNode
{
    struct BinTreeNode* left;   // 指向当前结点左孩子
    struct BinTreeNode* right;  // 指向当前结点右孩子
    BTDataType data;            // 当前结点值域
}BTNode;

BTNode* BuyNode(BTDataType x)
{
    BTNode* node = (BTNode*)malloc(sizeof(BTNode));
    if (node==NULL)
    {
        perror("malloc fail");
        return NULL;
    }
    node->data = x;
    node->left = NULL;
    node->right = NULL;
    return node;
}
void PreOrder(BTNode* root)
{
    if (root == NULL)
    {
        printf("NULL");
        return;

    }
    printf("%d", root->data);
    PreOrder(root->left);
    PreOrder(root->right);
}
void InOrder(BTNode* root)
{
    if (root == NULL)
    {
        printf("NULL");
        return;

    }
    InOrder(root->left);
    printf("%d", root->data);
    InOrder(root->right);
}
void PostOrder(BTNode* root)
{
    if (root == NULL)
    {
        printf("NULL");
        return;

    }
    PostOrder(root->left);
    PostOrder(root->right);
    printf("%d", root->data);

}
