#include"Tree.h"
int TreeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeNode(root->left) + Tree(root->right) + 1;
}
int TreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int leftHeight = TreeHeight(root->left);
	int rightHeight = TreeHeight(root->right);
	return TreeHeight(root->left) > TreeHeight(root->right) ? leftHeight + 1 : rightHeight + 1;
	 
}
int TreeKLevel(BTNode* root, int k)
{

	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;

	return TreeKLevel(root->left, k - 1)+ TreeKLevel(root->right, k - 1);
}
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root = NULL)
		return NULL;
	if (root->data == x)
		return root;
	BTNode* lret = BinaryTreeFind(root->left, x);
	if (lret)
		return lret;
	BTNode* rret = BinaryTreeFind(root->right, x);
	if (rret)
			return rret;
	return NULL;
}
BTNode* create(int data[], int n) {
	BTNode* root = NULL;     //新建空根结点
	for (int i = 0; i < n; i++) {
		insert(&root, data[i]); //将data[0]到data[n-1]插入二叉树
	}
	return root; //返回根节点
}

void insert(BTNode* root, int x) {  //注意 传入的是结点指针的引用
	if (root == NULL) { //空树，即查找失败，插入结点（递归边界）
		root = newNode(x);
		return;
	}
	if (root->data > x) { //往左子树搜索
		insert(root->left, x);
	}
	else insert(root->right, x); //往右子树搜索
}

int main() {


	
	return 0;
}