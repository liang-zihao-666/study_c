#include"Stack.h"
int main()
{
	ST st;
	STInit(&st);
	STPush(&st, 1);
	STPush(&st, 2);
	printf("%d\n", STTop(&st));
	STPush(&st, 3);
	STPush(&st, 4);
	printf("%d\n", STTop(&st));
	STPush(&st, 5);
	STPush(&st, 6);
	printf("%d\n", STTop(&st));

	while (!STEmpty(&st))
	{
		printf("%d", STTop(&st));
		STPop(&st);
	}
	STDestroy(&st);
	return 0;
}