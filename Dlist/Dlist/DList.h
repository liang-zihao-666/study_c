#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
typedef int LTNodeType;
typedef struct	ListNode
{
	struct ListNode* prev;
	struct ListNode* next;
	LTNodeType data;
}LTNode;
LTNode* LTInit();
void LTDestroy();
void LTPushBack(LTNode* phead, LTNodeType x);
void LTPopBack(LTNode* phead);

LTNode* BuyListNode(LTNodeType x);
void LTPrint(LTNode* phead);
bool LTEmpty(LTNode* phead);
void LTPushFront(LTNode* phead,LTNodeType x);
void LTPopFront(LTNode* phead);
//pos位置之前插入
void LTInsert(LTNode* pos, LTNodeType x);
//pos位置删除
void LTErase(LTNode* pos);
LTNode* LTFind(LTNode* phead, LTNodeType x);
void LTDestroy(LTNode* phead);