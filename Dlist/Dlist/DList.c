#include"DList.h"
void LTPushBack(LTNode* phead, LTNodeType x) {

	assert(phead);
	//phead     tail       newnode 
	LTNode* newnode = BuyListNode(x);
	LTNode* tail = phead->prev;
	tail->next = newnode;
	newnode->prev = tail;
	newnode->next = phead;
	phead->prev = newnode;
	//����
	//LTInsert(phead,x)
}
LTNode* LTInit() {
	LTNode* phead = BuyListNode(-1);
	phead->next =phead;
	phead->prev = phead;
	return phead;
}
LTNode* BuyListNode(LTNodeType x)
{
	LTNode* node = (LTNode*)malloc(sizeof(LTNode));
	if (node == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	node->next = NULL;
	node->prev = NULL;
	node->data = x;
	return node;
 }
void LTPrint(LTNode*phead)
{
	assert(phead);
	printf("<=head=>\n");
	LTNode* cur = phead->next;
	while (cur != phead)
	{
		printf("%d", cur->data);
		cur = cur->next;
	}
}
bool LTEmpty(LTNode* phead)
{
	assert(phead);
	return phead->next == phead;
}
void LTPopBack(LTNode* phead)
{
	assert(phead);
	assert(LTEmpty(phead));
	LTNode* tail = phead->prev;
	LTNode* pretail = tail->prev;
	pretail->next = phead;
	phead->prev = pretail;
	free(tail);
	tail = NULL;

	//����
	//LTErase��phead->prev��;
}

void LTPushFront(LTNode* phead, LTNodeType x)
{
	LTNode* newnode = BuyListNode(x);
	newnode->next = phead->next;
	phead->next->prev = newnode;
	phead->next = newnode;
	newnode->prev = phead;
	//����
	//LTInsert(phead->netx,x)

}
void LTPopFront(LTNode* phead) {
	assert(phead);
	assert(LTEmpty(phead));
	LTNode* mid = phead->next;
	LTNode* later = mid->next;
	phead->next = later;
	later->prev = phead;
	free(mid);
	mid = NULL;
	//����
	//LTErase(phead->next);

}
void LTInsert(LTNode* pos, LTNodeType x)
{
	assert(pos);
	LTNode* prev = pos->prev;
	LTNode* newnode = BuyListNode(x);
	prev->next = newnode;
	newnode->prev = prev;
	newnode->next = pos;
	pos->prev = newnode;
}
void LTErase(LTNode* pos)
{
	assert(pos);
	LTNode* prev = pos->prev;
	LTNode* next = pos->next;
	prev->next = next;
	next->prev = prev;
	free(pos);

}
LTNode* LTFind(LTNode* phead, LTNodeType x)
{
	assert(phead);
	LTNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next; 
	}
	return NULL;
}
void LTDestroy(LTNode* phead)
{
	assert(phead);
	LTNode* cur = phead->next;
	while (cur!=phead)
	{
		LTNode* next = cur->next;
		free(cur);
		cur =next;
	}
	free(phead);
	//phead = NULL;
}